package com.itau.Sistema.ViaCepModule.controller;

import com.itau.Sistema.ViaCepModule.LocalizacaoDTO;
import com.itau.Sistema.ViaCepModule.ViaCepModuleFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViaCepModuleController {

    @Autowired
    ViaCepModuleFeign viaCepModuleFeign;

    @GetMapping("/cep/{cep}")
    public LocalizacaoDTO formaEndereco(@PathVariable String cep) {
        return viaCepModuleFeign.pegaCep(cep);
    }

}
