package com.itau.Sistema.ViaCepModule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ViaCepModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ViaCepModuleApplication.class, args);
	}

}
