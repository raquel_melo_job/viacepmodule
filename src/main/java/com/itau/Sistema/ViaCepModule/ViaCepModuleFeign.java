package com.itau.Sistema.ViaCepModule;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "https://viacep.com.br/ws/")
public interface ViaCepModuleFeign {

    @GetMapping("/{cep}/json")
    LocalizacaoDTO pegaCep(@PathVariable String cep);
}
