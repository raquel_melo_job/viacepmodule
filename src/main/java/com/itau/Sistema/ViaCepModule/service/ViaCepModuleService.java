package com.itau.Sistema.ViaCepModule.service;

import com.itau.Sistema.ViaCepModule.LocalizacaoDTO;
import com.itau.Sistema.ViaCepModule.ViaCepModuleFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViaCepModuleService {

    @Autowired
    private ViaCepModuleFeign viaCepModuleFeign;

    public LocalizacaoDTO pegaCep(String cep){
        return viaCepModuleFeign.pegaCep(cep);
    }
}
